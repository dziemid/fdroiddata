Categories:Navigation
License:Apache2
Web Site:https://icecondor.com
Source Code:https://github.com/icecondor/android
Issue Tracker:https://github.com/icecondor/android/issues

Auto Name:IceCondor
Summary:Location tracking and sharing
Description:
Track your location all-day and share the data.
.

Repo Type:git
Repo:https://github.com/icecondor/android.git

Build:20141218,20141218
    commit=5780278fe6b47ac59296dc106bcd405d7e1b758a
    srclibs=1:appcompat@v7,2:AndroidAsync@42eabc6ca99c4011ed1ce30e4a7e77608f8d96fa,JodaTime@v2.5
    prebuild=pushd $$JodaTime$$ && \
        $$MVN3$$ package && \
        popd && \
        mkdir -p libs && \
        cp $$JodaTime$$/target/*.jar libs/

Build:20150301,20150301
    commit=f8ebe7a6cbc0e7fc48591759cabd3cda3c299b15
    srclibs=1:appcompat@v7,2:AndroidAsync@42eabc6ca99c4011ed1ce30e4a7e77608f8d96fa,JodaTime@v2.5
    prebuild=pushd $$JodaTime$$ && \
        $$MVN3$$ package && \
        popd && \
        mkdir -p libs && \
        cp $$JodaTime$$/target/*.jar libs/

Build:20150402,20150402
    commit=cd7038d8605237bfb1fdeb3346cbd04e8fd8bca3
    srclibs=1:appcompat@v7,2:AndroidAsync@dbf0be16e7004c3b175174d3f6a6e01fd02f7a06,JodaTime@v2.7
    prebuild=pushd $$JodaTime$$ && \
        $$MVN3$$ package && \
        popd && \
        mkdir -p libs && \
        cp $$JodaTime$$/target/joda-time-2.7.jar libs/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:20150402
Current Version Code:20150402

