Categories:Internet
License:GPLv3
Web Site:http://www.xabber.com
Source Code:https://github.com/redsolution/xabber-android
Issue Tracker:https://github.com/redsolution/xabber-android/issues

Name:Xabber
Auto Name:Xabber Development Version
Summary:Instant messaging client
Description:
Jabber (XMPP) client app with multiple accounts, privacy modes and a
clean and simple interface. #IM

Supported protocols:
* RFC-3920: Core;
* RFC-3921: Instant Messaging and Presence;
* XEP-0030: Service Discovery;
* XEP-0128: Service Discovery Extensions;
* XEP-0115: Entity Capabilities;
* XEP-0054: vcard-temp;
* XEP-0153: vCard-Based Avatars;
* XEP-0045: Multi-User Chat (incompletely);
* XEP-0078: Non-SASL Authentication;
* XEP-0138: Stream Compression;
* XEP-0203: Delayed Delivery;
* XEP-0091: Legacy Delayed Delivery;
* XEP-0199: XMPP Ping;
* XEP-0147: XMPP URI Scheme Query Components;
* XEP-0085: Chat State Notifications;
* XEP-0184: Message Delivery Receipts;
* XEP-0155: Stanza Session Negotiation;
* XEP-0059: Result Set Management;
* XEP-0136: Message Archiving;
* XEP-0224: Attention.

[https://github.com/redsolution/xabber-android/wiki/Xabber-Version-History Changelog]
.

Repo Type:git
Repo:https://github.com/redsolution/xabber-android.git

Build:0.9.29a,78
    commit=cbfe6da973b63ea36ea9e879ceaa057ad21624ec
    prebuild=git clone https://github.com/redsolution/otr4j && \
        ant jar -f otr4j/build.xml && \
        mv otr4j/bin/debug/jar/otr4j.jar libs/otr4j.jar
    target=android-10

Build:0.9.30a,80
    commit=f95362dd838a18691d5899c5
    target=android-10

Build:0.9.30b,81
    commit=2b8e6b216fa2a928b2f62b7cd62b2c079d706d38
    target=android-10

Build:0.9.30c,82
    commit=277806a50e665696ef1a176e96f94cc3487a92fc
    target=android-10

Build:0.9.30d,86
    commit=8bb32fcf2253d9b7a5fbc57e9c2d3cbb3f76cc5c
    subdir=app
    gradle=yes
    srclibs=Otr4j@7fb42b2ef912fe664064edf717a6b72ce77ccb3b
    rm=app/libs/*.jar
    prebuild=sed -i -e '/otr4j.jar/d' build.gradle && \
        cp -fR $$Otr4j$$/src/net src/main/java

Build:0.9.30e,88
    commit=0.9.30e
    subdir=app
    gradle=yes
    srclibs=Otr4j@7fb42b2ef912fe664064edf717a6b72ce77ccb3b
    rm=app/libs/*.jar
    prebuild=sed -i -e '/otr4j.jar/d' build.gradle && \
        cp -fR $$Otr4j$$/src/net src/main/java

Build:0.9.30f,102
    commit=0.9.30f
    subdir=app
    gradle=yes
    srclibs=Otr4j@7fb42b2ef912fe664064edf717a6b72ce77ccb3b
    rm=app/libs/*.jar
    prebuild=sed -i -e '/otr4j.jar/d' build.gradle && \
        cp -fR $$Otr4j$$/src/net src/main/java

Build:0.10.10,107
    disable=dont overflow archive policy
    commit=ef08f37400
    subdir=app
    gradle=yes
    srclibs=Otr4j@7fb42b2ef912fe664064edf717a6b72ce77ccb3b
    rm=app/libs/*.jar
    prebuild=sed -i -e '/otr4j.jar/d' build.gradle && \
        cp -fR $$Otr4j$$/src/net src/main/java

Build:0.10.17,114
    disable=dont overflow archive policy
    commit=de96112a28
    subdir=app
    gradle=yes
    srclibs=Otr4j@7fb42b2ef912fe664064edf717a6b72ce77ccb3b
    rm=app/libs/*.jar
    prebuild=sed -i -e '/otr4j.jar/d' build.gradle && \
        cp -fR $$Otr4j$$/src/net src/main/java

Build:0.10.18,115
    disable=dont overflow archive policy
    commit=d5cdd17976
    subdir=app
    gradle=yes
    srclibs=Otr4j@7fb42b2ef912fe664064edf717a6b72ce77ccb3b
    rm=app/libs/*.jar
    prebuild=sed -i -e '/otr4j.jar/d' build.gradle && \
        cp -fR $$Otr4j$$/src/net src/main/java

Build:0.10.19,116
    disable=dont overflow archive policy
    commit=ebcde01b6e
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.21,118
    disable=dont overflow archive policy
    commit=9645ca0f35dab7206dc500681e2d8d4aff3509a
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.22,119
    disable=dont overflow archive policy
    commit=8f43ee8696e4d9af17d0e40b18fdc71d95f57d6
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.23,120
    disable=dont overflow archive policy
    commit=baf141da69f1f18c22ec152360719f25bc97fa11
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.23a,123
    disable=dont overflow archive policy
    commit=b273a762cf89cac486f117cc90c46cb4394de0b6
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.31,130
    disable=dont overflow archive policy
    commit=dc29dedd19774179e1afb2a82ee3fe26771c4d
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.33,132
    disable=dont overflow archive policy
    commit=1e3997c3a31889b72e644899a983b2fdc434b018
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.35,135
    disable=dont overflow archive policy
    commit=6b384648d838613cd5f1d46730492617e2dd42ae
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.36,136
    disable=dont overflow archive policy
    commit=238a129283b4aeceb0250abde3dff7bd91a6b9a0
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.45,145
    commit=334f38fc52be5ed62c1154e6ef837371c893f41f
    subdir=app
    gradle=yes
    submodules=yes

Build:0.10.62,162
    commit=cb05547c84f9a286de5894de1498385ab92fcece
    subdir=app
    gradle=yes
    submodules=yes

Maintainer Notes:
* Untagged 0.10.10+ builds are pre-releases from the "develop" branch. Don't
  switch to UCM:RepoManifest/develop, we want the CV to be in sync with the
  releases.
* Depending on the release cycle, we have to make sure does current CV is
  in the main repo and hasn't been sent to archive. I suggest we keep 4 builds
  and only have one active pre-release version (disable the old ones to remove
  the apk).
.

Archive Policy:4 versions
Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.9.30f
Current Version Code:102

