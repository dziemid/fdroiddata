Categories:Multimedia
License:GPLv3
Web Site:https://olejon.github.io/spotcommander
Source Code:https://github.com/olejon/spotcommander
Issue Tracker:https://github.com/olejon/spotcommander/issues
Donate:http://www.olejon.net/code/spotcommander/?donate

Auto Name:SpotCommander
Summary:Remote control for Spotify for Linux
Description:
Remote control Spotify for Linux. For this to work, your have to install
[http://www.olejon.net/code/spotcommander/?install SpotCommander] on your
regular PC first.
.

Repo Type:git
Repo:https://github.com/olejon/spotcommander

Build:4.7,47
    commit=c468892825cc300889fcb105223e10f271745e98
    subdir=android/app
    gradle=yes

Build:4.8,48
    commit=df0858c0c35c753d5fa338425e477174fa2271cd
    subdir=android/app
    gradle=yes

Build:4.9,49
    commit=6bb20e9cf0a73665b9d2583eb3267b34d0ed3c16
    subdir=android/app
    gradle=yes

Build:5.0,50
    commit=cc2a256055a7b5a0ec389ef16eb49d98815ca772
    subdir=android/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:5.0
Current Version Code:50

