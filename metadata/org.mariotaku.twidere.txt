Categories:Internet
License:GPLv3
Web Site:
Source Code:https://github.com/TwidereProject/Twidere-Android
Issue Tracker:https://github.com/TwidereProject/Twidere-Android/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=mariotaku.lee%40gmail%2ecom&item_name=Donate%20to%20Twidere
Bitcoin:1FHAVAzge7cj1LfCTMfnLL49DgA3mVUCuW

Auto Name:Twidere
Summary:Microblogging client
Description:
Features include:
* Built in image viewer and map viewer
* Direct messages with conversation style
* Extension support like Twicca, but more friendly for users and developers
* t.co link auto expanding
* Multiple account support
* Custom oAuth consumer key/secret
* Basic features supported for StatusNet/Fanfou
* Direct login with user name and password (Don't worry! It's safe!)
* Custom API Proxy (gtap, twip, jtapi supported)
* GZip compressing for API requests
* TCP DNS query and custom host mapping to fight against censorship

Development is focussed on Twitter and that service is the only one officially
supported.

If you want a StatusNet account press the button at the top of the account
setup page.
Set an appropriate REST API url and choose basic authentication.
Searches will go to Twitter by default.
For general info about the StatusNet API see
[http://status.net/wiki/Twitter-compatible_API their wiki].

You can donate via PayPal here, with the same address being used for AliPay
donations.
.

Repo Type:git
Repo:https://github.com/TwidereProject/Twidere-Android

#Repo:https://github.com/mariotaku/twidere.git
Build:0.2.9.5,81
    disable=repo changed
    commit=0.2.9.5
    init=rm libs/android-support-v13.jar
    srclibs=1:ActionBar-PullToRefresh@v0.9.1,2:DragSort@0.6.1,3:SlidingMenu@7ebe32772
    extlibs=android/android-support-v4.jar,android/android-support-v13.jar
    prebuild=rm -rf dist/ art/ && \
        cp libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp libs/android-support-v4.jar $$DragSort$$/libs/

Build:0.2.9.6-update2,84
    disable=repo changed
    commit=13811314f8ab9f14
    init=rm libs/android-support-v13.jar
    srclibs=1:ActionBar-PullToRefresh@v0.9.1,2:DragSort@0.6.1,3:SlidingMenu@7ebe32772
    extlibs=android/android-support-v4.jar,android/android-support-v13.jar
    prebuild=rm -rf dist/ art/ && \
        cp libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp libs/android-support-v4.jar $$DragSort$$/libs/

Build:0.2.9.7-update1,86
    disable=repo changed
    commit=da803eb75188eb9
    init=rm libs/android-support-v13.jar
    srclibs=1:ActionBar-PullToRefresh@v0.9.1,2:DragSort@0.6.1,3:SlidingMenu@7ebe32772
    extlibs=android/android-support-v4.jar,android/android-support-v13.jar
    prebuild=rm -rf dist/ art/ && \
        cp libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp libs/android-support-v4.jar $$DragSort$$/libs/

Build:0.2.9.8-update2,90
    disable=missing resources
    commit=b88ae6c3d3
    init=rm libs/android-support-v13.jar
    srclibs=1:ActionBar-PullToRefresh@v0.9.1,2:DragSort@0.6.1,3:SlidingMenu@7ebe32772
    extlibs=android/android-support-v4.jar,android/android-support-v13.jar
    prebuild=rm -rf dist/ art/ && \
        cp libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp libs/android-support-v4.jar $$DragSort$$/libs/

Build:0.2.9.9,91
    disable=repo changed
    commit=8a07f3a809
    init=rm libs/android-support-v13.jar
    srclibs=1:ActionBar-PullToRefresh@v0.9.2,2:DragSort@0.6.1,3:SlidingMenu@d929476be,4:HoloAccent@v0.9,5:MenuComponent@ee3d4,6:SmoothProgressBar@v0.3.2
    rm=dist,art
    extlibs=android/android-support-v13.jar
    prebuild=cp $$HoloAccent$$/libs/android-support-v4.jar $$DragSort$$/libs/ && \
        cp $$HoloAccent$$/libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp $$HoloAccent$$/libs/android-support-v4.jar $$MenuComponent$$/libs/

Build:0.2.9.10,92
    disable=repo changed
    commit=8858369
    init=rm libs/android-support-v13.jar
    srclibs=SmoothProgressBar@0.3.3,1:RefreshNow@3090bcde2a,2:DragSort@0.6.1,3:SlidingMenu@d929476be,4:HoloAccent@v0.9,5:MenuComponent@b52ff0493,6:AndroidStaggeredGrid@1.0.3
    rm=dist,art
    extlibs=android/android-support-v13.jar
    prebuild=cp $$HoloAccent$$/libs/android-support-v4.jar $$DragSort$$/libs/ && \
        cp $$HoloAccent$$/libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp $$HoloAccent$$/libs/android-support-v4.jar $$MenuComponent$$/libs/ && \
        mkdir $$AndroidStaggeredGrid$$/libs/ && \
        cp $$HoloAccent$$/libs/android-support-v4.jar $$AndroidStaggeredGrid$$/libs/
    target=android-19

#old repo
Build:0.2.9.12,94
    commit=503153f4ec
    patch=HomeActivity.patch
    srclibs=SmoothProgressBar@0.4.0,1:RefreshNow@edea0165172e3,2:DragSort@0.6.1,3:SlidingMenu@4254feca3ec,4:HoloAccent@v0.9.1,5:MenuComponent@2caf41895e,6:AndroidStaggeredGrid@1.0.4,7:ViewPagerIndicator@2.4.1
    rm=dist,art
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$DragSort$$/libs/ && \
        cp libs/android-support-v4.jar $$SlidingMenu$$/libs/ && \
        cp libs/android-support-v4.jar $$MenuComponent$$/libs/ && \
        mkdir $$AndroidStaggeredGrid$$/libs/ && \
        cp libs/android-support-v4.jar $$AndroidStaggeredGrid$$/libs/ && \
        cp libs/android-support-v4.jar $$ViewPagerIndicator$$/libs/
    target=android-19

#new repo
Build:0.3.0-dev-fdroid,98
    commit=49742e744692a213668a7855f75148b5f00877a0
    subdir=twidere
    submodules=yes
    gradle=fdroid
    srclibs=CWACMerge@v1.0.1,CWACSacklist@v1.0.1
    rm=twidere.wear,libraries/SlidingMenu/example
    prebuild=sed -i -e '/googleCompile/d' build.gradle && \
        cp -fR $$CWACMerge$$/merge/src/com src/main/java/ && \
        cp -fR $$CWACSacklist$$/sacklist/src/com src/main/java && \
        pushd libs && \
        jar xf jsonserializer-*.jar && \
        jar xf dnsjava-ipv6-1.0-with-sources.jar && \
        find -regex ".+\.\(a\|dll\|so\|exe\|lib\|class\)" -type f -delete && \
        cp -fR org ../src/main/java/ && \
        popd && \
        rm -fR libs && \
        echo -e "allprojects {\nsourceCompatibility = 1.7\ntargetCompatibility = 1.7\n}" >> ../build.gradle

Build:0.3.0-dev-fdroid,106
    disable=crash after login
    commit=8eb4ee994bd3671ba698c48d3d69df39f5513508
    subdir=twidere
    submodules=yes
    gradle=fdroid
    srclibs=CWACMerge@v1.0.1,CWACSacklist@v1.0.1,MariotakuDragSort@0.6.1,MariotakuMessageBubbleView@1.0,MariotakuSlidingMenu@501a9988a4d95958fe634fc09d5dc332de7830b9,ColorPicker-uucky@0.9.1
    rm=twidere.wear,twidere.extension.push.xiaomi,twidere.donate.nyanwp.wear,libraries/SlidingMenu/example
    prebuild=sed -i -e '/googleCompile/d' -e '/amazonaws/d' -e '/jitpack/d'  build.gradle && \
        cp -fR $$CWACMerge$$/merge/src/com src/main/java/ && \
        cp -fR $$CWACSacklist$$/sacklist/src/com src/main/java && \
        sed -i -e '/xujiaao/d' ../build.gradle && \
        sed -i -e '/wearApp/d' ../twidere.donate.nyanwp/build.gradle && \
        mkdir -p ../libraries/colorpicker && \
        cp -fR $$ColorPicker-uucky$$ ../libraries/colorpicker && \
        mkdir -p ../libraries/dragsort && \
        cp -fR $$MariotakuDragSort$$ ../libraries/dragsort && \
        mkdir -p ../libraries/slidingmenu && \
        cp -fR $$MariotakuSlidingMenu$$ ../libraries/slidingmenu && \
        mkdir -p ../libraries/messagebubbleview && \
        cp -fR $$MariotakuMessageBubbleView$$ ../libraries/messagebubbleview && \
        echo -e "\ninclude ':libraries:colorpicker:library'\ninclude ':libraries:dragsort:library'\ninclude ':libraries:slidingmenu:library'\ninclude ':libraries:messagebubbleview:library'" >> ../settings.gradle && \
        sed -i -e '/ColorPicker/acompile project(":libraries:colorpicker:library")' -e '/DragSortListView/acompile project(":libraries:dragsort:library")' -e '/SlidingMenu/acompile project(":libraries:slidingmenu:library")' build.gradle -e '/MessageBubbleView/acompile project(":libraries:messagebubbleview:library")' && \
        sed -i -e '/ColorPicker/d' -e '/DragSortListView/d' -e '/SlidingMenu/d' -e '/MessageBubbleView/d' -e '/commonsware/d' build.gradle && \
        sed -i -e '/versionNameSuffix String/aversionNameSuffix "-dev-fdroid"' build.gradle && \
        sed -i -e '/versionNameSuffix String/d' build.gradle

Build:0.3.0-dev-fdroid,107
    commit=bd0198a6237940c56063723f8ff221553b179dc0
    subdir=twidere
    submodules=yes
    gradle=fdroid
    srclibs=CWACMerge@v1.0.1,CWACSacklist@v1.0.1,MariotakuDragSort@0.6.1,MariotakuMessageBubbleView@1.0,MariotakuSlidingMenu@501a9988a4d95958fe634fc09d5dc332de7830b9,ColorPicker-uucky@0.9.1
    rm=twidere.wear,twidere.extension.push.xiaomi,twidere.donate.nyanwp.wear,libraries/SlidingMenu/example
    prebuild=sed -i -e '/googleCompile/d' -e '/amazonaws/d' -e '/jitpack/d'  build.gradle && \
        cp -fR $$CWACMerge$$/merge/src/com src/main/java/ && \
        cp -fR $$CWACSacklist$$/sacklist/src/com src/main/java && \
        sed -i -e '/xujiaao/d' ../build.gradle && \
        sed -i -e '/wearApp/d' ../twidere.donate.nyanwp/build.gradle && \
        mkdir -p ../libraries/colorpicker && \
        cp -fR $$ColorPicker-uucky$$ ../libraries/colorpicker && \
        mkdir -p ../libraries/dragsort && \
        cp -fR $$MariotakuDragSort$$ ../libraries/dragsort && \
        mkdir -p ../libraries/slidingmenu && \
        cp -fR $$MariotakuSlidingMenu$$ ../libraries/slidingmenu && \
        mkdir -p ../libraries/messagebubbleview && \
        cp -fR $$MariotakuMessageBubbleView$$ ../libraries/messagebubbleview && \
        echo -e "\ninclude ':libraries:colorpicker:library'\ninclude ':libraries:dragsort:library'\ninclude ':libraries:slidingmenu:library'\ninclude ':libraries:messagebubbleview:library'" >> ../settings.gradle && \
        sed -i -e '/ColorPicker/acompile project(":libraries:colorpicker:library")' -e '/DragSortListView/acompile project(":libraries:dragsort:library")' -e '/SlidingMenu/acompile project(":libraries:slidingmenu:library")' build.gradle -e '/MessageBubbleView/acompile project(":libraries:messagebubbleview:library")' && \
        sed -i -e '/ColorPicker/d' -e '/DragSortListView/d' -e '/SlidingMenu/d' -e '/MessageBubbleView/d' -e '/commonsware/d' build.gradle && \
        sed -i -e '/versionNameSuffix String/aversionNameSuffix "-dev-fdroid"' build.gradle && \
        sed -i -e '/versionNameSuffix String/d' build.gradle

Build:0.3.0-dev-fdroid,108
    commit=86ec5e3f4f6aaa92dce037e12f438df4bba6281c
    subdir=twidere
    submodules=yes
    gradle=fdroid
    srclibs=CWACMerge@v1.0.1,CWACSacklist@v1.0.1,MariotakuDragSort@0.6.1,MariotakuMessageBubbleView@1.0,MariotakuSlidingMenu@501a9988a4d95958fe634fc09d5dc332de7830b9,ColorPicker-uucky@0.9.3
    rm=twidere.wear,twidere.extension.push.xiaomi,twidere.donate.nyanwp.wear,libraries/SlidingMenu/example
    prebuild=sed -i -e '/googleCompile/d' -e '/amazonaws/d' -e '/jitpack/d'  build.gradle && \
        cp -fR $$CWACMerge$$/merge/src/com src/main/java/ && \
        cp -fR $$CWACSacklist$$/sacklist/src/com src/main/java && \
        sed -i -e '/xujiaao/d' ../build.gradle && \
        sed -i -e '/wearApp/d' ../twidere.donate.nyanwp/build.gradle && \
        mkdir -p ../libraries/colorpicker && \
        cp -fR $$ColorPicker-uucky$$ ../libraries/colorpicker && \
        mkdir -p ../libraries/dragsort && \
        cp -fR $$MariotakuDragSort$$ ../libraries/dragsort && \
        mkdir -p ../libraries/slidingmenu && \
        cp -fR $$MariotakuSlidingMenu$$ ../libraries/slidingmenu && \
        mkdir -p ../libraries/messagebubbleview && \
        cp -fR $$MariotakuMessageBubbleView$$ ../libraries/messagebubbleview && \
        echo -e "\ninclude ':libraries:colorpicker:library'\ninclude ':libraries:dragsort:library'\ninclude ':libraries:slidingmenu:library'\ninclude ':libraries:messagebubbleview:library'" >> ../settings.gradle && \
        sed -i -e '/ColorPicker/acompile project(":libraries:colorpicker:library")' -e '/DragSortListView/acompile project(":libraries:dragsort:library")' -e '/SlidingMenu/acompile project(":libraries:slidingmenu:library")' build.gradle -e '/MessageBubbleView/acompile project(":libraries:messagebubbleview:library")' && \
        sed -i -e '/ColorPicker/d' -e '/DragSortListView/d' -e '/SlidingMenu/d' -e '/MessageBubbleView/d' -e '/commonsware/d' build.gradle && \
        sed -i -e '/versionNameSuffix String/aversionNameSuffix "-dev-fdroid"' build.gradle && \
        sed -i -e '/versionNameSuffix String/d' build.gradle

Maintainer Notes:
* JonSerializer issues when using srclib, see: https://github.com/TwidereProject/Twidere-Android/issues/37
* We might want to install CWAC into mavenLocal. Use this instead of copying:
sed -i -e '/googleCompile/d' -e "/twitter-text/acompile 'com.commonsware.cwac:merge:1.0.1'\ncompile 'com.commonsware.cwac:sacklist:1.0.0'" build.gradle && \
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.3.0
Current Version Code:108

