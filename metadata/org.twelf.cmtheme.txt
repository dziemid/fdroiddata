Categories:System
License:GPLv3
Web Site:https://gitlab.com/xphnx/twelf_cm12_theme/blob/HEAD/README.md
Source Code:https://gitlab.com/xphnx/twelf_cm12_theme
Issue Tracker:https://gitlab.com/xphnx/twelf_cm12_theme/issues

Auto Name:TwelF
Summary:CM12 theme for devices using only FLOSS apps
Description:
Your ROM must include the CM12 Theme Engine. This theme contains exclusively
icons for apps hosted on F-Droid FLOSS Repository.

TwelF follows the guidelines of Google Material Design for Android Lollipop
(Android 5) aiming to provide a unified and minimalistic look at devices
using CM12 with only Libre or Open Source Apps.

For issues, comments and icon request, please use the issue tracker.

Status: Alpha

[https://gitlab.com/xphnx/twelf_cm12_theme/blob/HEAD/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://gitlab.com/xphnx/twelf_cm12_theme.git

Build:0.1,1
    disable=remove apk
    commit=v0.1
    subdir=theme
    gradle=yes

Build:0.2,2
    commit=v0.2
    subdir=theme
    gradle=yes

Build:0.3,3
    commit=v0.3
    subdir=theme
    gradle=yes

Build:0.4,4
    commit=v0.4
    subdir=theme
    gradle=yes

Build:0.7,7
    commit=v0.7
    subdir=theme
    gradle=yes

Build:0.8,8
    commit=1903b59c91a73a38bcb5591325c4efbd71a4235d
    subdir=theme
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.8
Current Version Code:8